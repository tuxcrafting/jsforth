fetch-text = (url) ->
  (res) <- fetch url .then
  res.text!

window.add-event-listener \load, (ev) !->
  (prelude) <-! fetch-text 'prelude.f' .then
  (index) <-! fetch-text 'index.f' .then
  jsf = new JSFORTH
  jsf.evaluate-lines prelude
  jsf.evaluate-lines index
