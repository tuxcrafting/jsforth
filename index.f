: query-selector" ij" 1
    [window]. document..
    [window]. document.querySelector.. jcall ;

query-selector" #canvas" constant canvas
ij" 2d" 1 canvas canvas prop. getContext.. jcall constant ctx

: canvas-width ( -- n ) canvas [prop]. width.. ;
: canvas-height ( -- n ) canvas [prop]. height.. ;

: canvas-fill-style ( obj -- ) ctx property!" fillStyle" ;

: canvas-clear ( -- )
    j" #000000" canvas-fill-style
    0 0 canvas-width canvas-height
    4 ctx ctx [prop]. fillRect.. jcall drop ;

16 constant ball-size
variable ball-x 0 ball-x !
variable ball-y 0 ball-y !
variable ball-dx 5 ball-dx !
variable ball-dy 3 ball-dy !

: draw-ball ( -- )
    j" #FF0000" canvas-fill-style
    ball-x @ ball-y @
    ball-size ball-size
    4 ctx ctx [prop]. fillRect.. jcall drop ;

: bbox-width canvas-width ball-size - ;
: bbox-height canvas-height ball-size - ;

: update-ball ( -- )
    ball-dx @ ball-x +!
    ball-dy @ ball-y +!
    ball-x @ 0 <
    ball-x @ bbox-width >
    or if ball-dx @ negate ball-dx ! then
    ball-y @ 0 <
    ball-y @ bbox-height >
    or if ball-dy @ negate ball-dy ! then ;

:noname drop drop
    canvas-clear
    draw-ball
    update-ball
    null ;
jexecute 20 2
window window. setInterval.. jcall drop
